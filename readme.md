# Jasper

Statically typed, compile-time checked dependency injection using higher-kinded monad types in Java.

# Status

Example of the workflow compiles.

# Goals

- Easily interoperate with spring framework
- Minimize boilerplate
- Encourage monadic handling of non-determinism (errors, nullability, etc)

# Example

See the first example [here](src/test/java/examples/Example.java)

# What is this and Why?

Spring bothers me. It's super convenient, and I'm not advocating anybody stop using it (yet), but Spring's primary model is that we go through the program *at run-time* and build a model of what needs what, and then we wire all that stuff together. **At run-time**. Using reflection. Lots of people don't have a problem with using run-time reflection. I do. So I want a different way to do things.

Turns out, there are at least two ways.

[dagger](https://github.com/google/dagger) exemplifies one way, and follows the path taken by tools like [lombok](https://projectlombok.org) and [mapstruct](https://mapstruct.org/), which is to say they all use compile-time code generation. Lots of people are okay with compile-time code generation. I'm one of them, and I encourage people building java applications to USE LOMBOK. Seriously. It's that good. Use it. It will save you a huge amount of effort and time. But I'm also an explorer at heart, and I think functional programming is cool, so I went looking for yet another way.

The other way is with monads. 

### Monads in Monadville

"Monads!" I hear you cry. "Why did it have to be monads?" But I actually think this approach has advantages over the approaches above. It has some disadvantages, like, wtf is a `Kleisli` for god's sake? I'll get to those. But I think it has advantages as well. Let me start with a simplistic example. Consider an application whose api provides two functions:

- Save a user to a database
- Fetch a user from the database

The database could be mysql, postgres, sqlite, we don't care. I'll use false names for database-related stuff, because this is just meant to be an introductory example. I also will not implement a controller, but I will implement a Service class, and you'll notice I do something a little strange there.

```java

# Model.java
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Value;

public class Model {
    @Value(staticConstructor = "of") // <- See the lombok? It's so nice.
    public static class ID {
        int id;                      // Go ahead. You can touch it.
    }

    @Data
    @AllArgsConstructor
    public static class User implements DB.Record<ID> {
        String name;
        ID id;
    }
}


# Service.java
public class Service {
    public static Function<? extends DB.Put<ID, User>, User> addUser(User user) {
        return db -> db.put(user);
    }
    
    public static Function<? extends DB.GetSingleBy1<ID, ID, User>, User> getUser(ID id) {
        return db -> db.get(id);
    }
}

# DBContext.java
import lombok.Value;

@Value(staticConstructor = "connect")
public class DBContext
        implements
        DB.GetSingleBy1<ID, ID, User>,
        DB.Put<ID, User> {
    private final String connectionString;
    private final DBConnection conn = Database.connect(connectionString);
    
    public User get(ID id) {
        return conn.getByID(id);
    }
    
    public User put(User user) {
        conn.put(user);
        return user;
    }
}

# Main.java
public class Main {
    public static void main() {
        DBContext dbContext = DBContext.connect("sqlite:///local.db")
    
        User user = new User(
            "John Woo",
            ID.of(1)
        );

        User result = Service.addUser(user)
          .map(User::getId)
          .bind(Service::getUser)
          .apply(dbContext)
        
        System.out.println(result.toJSON());
        
        /*
        Should print out:
        {"name": "John Woo", "id": {"id": 1}}
        */
    }
}
```

Notice how all the service methods are static methods? Each takes a typical webservice argument, like a user or an id, but instead of doing anything with that datum, each return a closure over it. Each closure is a function that asks for specific functionality related to the database: `GetSingleBy1` or `Put`. When given that functionality, each closure uses it on the closed-over datum (the user or id) and returns the result.

This means a couple of things that, to me, are pretty interesting. In the function that cares about fetching users from a database, we -only- ask for the functionality we need. We don't ask for the entire database worth of functionality, all we ask for is a way to get a user out of it. Likewise, in the function that cares about sticking users into the database, we -only- ask for a way to stick a user into the database. That's compelling to me, because it fits neatly into my mental model where Separation of Concerns (tm) is important, and it allows me to determine the sum of what I need based on what my functions explicitly ask for at the point of need.

I can compose functionaliy out of parts, too. Consider an example where I need to get a User from the User table and an Account from the Account table:

```java
import jasper.data.DB;

public class UserAccountService {
    public static interface DBGetUserById extends DB.GetSingleBy1<UserId, UserId, User> {
        default User getUser(UserId id){
            return get(id);
        }
    }
    
    public static interface DBGetAccountById extends DB.GetSingleBy1<AccountId, AccountId, Account> {
        default Account getAccount(AccountId id) {
            return get(id);
        }
    }

    public static Function<? extends DBGetUserById & DBGetAccountById, Account> getUserAccount(UserId userId) {
        return db -> {
            User user = db.getUser(userId);
            Account account = db.getAccount(user.accountId);
            return account;
        }
    }
}
```

That's not -super- pretty, because type erasure is a thing, and I'm still working out how I want to get around that, but I think my point stands. I can compose my specific needs at point of call, and if I pass in a `DBContext` object that doesn't satisfy them all, my code won't compile.

Additionally, notice that I don't have to instantiate and keep track of a `Service` instance. I can just call its methods from anywhere I want. Since `DB.Put` and `DB.GetSingleBy1` are both `@FunctionalInterface`s with a single method, I can even *test* my `Service` methods by supplying lambda expressions that satisfy the interface in question. No mocks! They can throw exceptions. They can return shitty data. They can loop to infinity. I can test any case I want, and the only thing I have to change in order to have different testing behavior is what lambda I use to satisfy the interface in question.

Now, note that what we see above won't compile anyway, for a number of reasons. Only one of those reasons is unfixable. I've used a fake `Database`, because frankly I don't care about the details of the database backend and neither should you. But since I'm lazy and haven't supplied one that in fact exists, this example won't compile. For the rest:

#### Binding Agreements

First, `map` and `bind` don't exist on `Function`. Each describes functionality that is conceptually common across a fairly wide variety of different types. Let's start with `map`. Take, for example, a static definition of `map` for a known concrete type:

```java
import java.util.ArrayList;
import java.util.function.Function;

public class Mapping {
    public static <T, R> ArrayList<R> map(ArrayList<T> list, Function<? super T, ? extends R> fn) {
        ArrayList<R> res = new ArrayList<>();
        list.forEach(t -> res.add(fn.apply(t)));
        return res;
    }
}
```

We can trivially define that `map` function for any concrete collection type:

```java
import java.util.ArrayList;
import java.util.function.Function;
import java.util.HashMap;
import java.util.HashSet;

public static class Mapping {
    public static <T, R> ArrayList<R> map(ArrayList<T> list, Function<? super T, ? extends R> fn) {
        ArrayList<R> res = new ArrayList<>();
        list.forEach(t -> res.add(fn.apply(t)));
        return res;
    }

    public static <K, T, R> HashMap<K, R> map(HashMap<K, T> map, Function<? super T, ? extends R> fn) {
        HashMap<K, R> res = new HashMap<>();
        map.forEach((k, t) -> res.put(k, fn.apply(t)));
        return res;
    }

    public static <T, R> HashSet<R> map(HashSet<T> set, Function<? super T, ? extends R> fn) {
        HashSet<R> res = new HashSet<>();
        set.forEach(t -> res.add(fn.apply(t)));
        return res;
    }
}
```

But we can *also* define it for *any* type that can be conceptually thought of as containing another type. `Function`, for example:

```java
import java.util.function.Function;

public static class Mapping {
    public static <I, T, R> Function<I, R> map(Function<? super I, ? extends T> fn, Function<? super T, ? extends R> mapper) {
        return i -> mapper.apply(fn.apply(i));
    }
}
```

In fact, `Function` has this already; it's called `andThen`. From the [source](https://github.com/openjdk/jdk/blob/master/src/java.base/share/classes/java/util/function/Function.java):

```java
@FunctionalInterface
public interface Function<T, R> {
    
    ...

    default <V> Function<T, V> andThen(Function<? super R, ? extends V> after) {
        Objects.requireNonNull(after);
        return (T t) -> after.apply(apply(t));
    }
}
```

But why is it called `andThen`? `andThen` only applies in the context of the *doing-ness* represented by a `Function`. By observing the generalizability of this concept, we can give it a more generalizable name; if it has a name, we can give it a type or an interface. We might say a thing is `Mappable`:

```java
import java.util.function.Function;

public interface Mappable<T> {
    public <R> Mappable<R> map(Function<? super T, ? extends R> fn);
}
```

In fact, this concept already has a name. In category theory a thing that is mappable is known as a `Functor`. That is, the stuff it "contains" can be submitted to a `Function`, and a result gotten, without any change being made to the containing object (the `Functor`):

```java
public interface Functor<T> {
    public <R> Functor<R> map(Function<? super T, ? extends R> fn);
}
```

We can follow a similar process for `bind`. `bind` represents a layer of abstraction around `Functor`. Using the term `Bindable` for the moment, `bind` represents the concept that we can take the contents of a `Bindable<T>`, pass them into a function, and get back a new `Bindable<R>`:

```java
public interface Bindable<T> {
    public <R> Bindable<R> bind(Function<? super T, ? extends Bindable<? extends R>>);
}
```

We can define `bind` for various things just as we did for `map` above:

```java
public static class Binding {
    public static <T, R> ArrayList<R> bind(ArrayList<T> list, Function<? super T, ? extends ArrayList<R>> fn) {
        ArrayList<R> res = new ArrayList<>();
        list.forEach(t -> res.addAll(fn.apply(t)));
        return res;
    }

    public static <K, T, R> HashMap<K, R> bind(HashMap<K, T> map, Function<? super T, ? extends HashMap<K, R>> fn) {
        HashMap<K, R> res = new HashMap<>();
        map.forEach((k, t) -> res.putAll(fn.apply(t)));
        return res;
    }

    public static <T, R> HashSet<R> bind(HashSet<T> set, Function<? super T, ? extends HashSet<R>> fn) {
        HashSet<R> res = new HashSet<>();
        set.forEach(t -> res.addAll(fn.apply(t)));
        return res;
    }

    public static <I, T, R> Function<? super I, ? extends R> map(Function<? super I, ? extends T> fn, Function<? super T, ? extends Function<? super I, ? extends R>> binder) {
        return i -> binder.apply(fn.apply(i)).apply(i);
    }
}
```

If you're familiar with the java `Streams` api, or with other languages outside Haskell that offer this functionality, you probably know it as `flatMap`. This too, this concept of being able to bind the contents of a `Thing` and return a new `Thing` with different contents, has a name from category theory. In category theory, it is called `Monad`:

```
public interface Monad<T> extends Functor<T> {
    public <R> Monad<R> bind(Function<? super T, ? extends Monad<? extends R>);
}
```

And we're using it above to make it so that our Service functions don't have to know about eachother, or have versions that know about eachother. Without `bind`, our `main` function would have to rely on `map` alone. We can do this semi-conveniently, if we assume the presence of a `Tuple2` class, which gives us tools like `mapFirst` and `mapSecond` to update its contents:

```java
    User result = Service.addUserBegin(user)
        .mapFirst(User::getIdAnd)
        .map(Service::getUserFinish)
        .apply(dbContext)
```

In order to facilitate that, we would have to write context-managing versions of our Service functions:

```java
# Service.java
public class Service {
    public static <D extends DB.Put<ID, User>> Function<D, Tuple2<D, User>> addUserBegin(User user) {
        return db -> Tuple.of(db, db.put(user));
    }
    
    public static <D extends DB.GetSingleBy1<ID, ID, User>> Function<Tuple2<D, User>, User> getUserFinish(ID id) {
        return tuple -> tuple.first().get(id);
    }
}
```

But, because we can't know what order our functions are going to be called in at the `Service` level, we would then have to write both the inverse and middle of each of those steps:

```java
# Service.java
public class Service {
    public static <D extends DB.Put<ID, User>> Function<D, Tuple2<D, User>> addUserBegin(User user) {
        return db -> Tuple.of(db, db.put(user));
    }
    
    public static <D extends DB.Put<ID, User>> Function<Tuple2<D, User>, Tuple2<D, User>> addUserContinue(User user) {
        return tuple -> Tuple.of(tuple.first(), tuple.first().put(user));
    }
    
    public static <D extends DB.Put<ID, User>> Function<Tuple2<D, User>, User> addUserFinish(User user) {
        return tuple -> tuple.first().put(user);
    }
    
    public static <D extends DB.GetSingleBy1<ID, ID, User>> Function<D, Tuple2<D, User>> getUserBegin(ID id) {
        return db -> Tuple.of(db, db.get(id));
    }
    
    public static <D extends DB.GetSingleBy1<ID, ID, User>> Function<Tuple2<D, User>, Tuple2<D, User>> getUserContinue(ID id) {
        return tuple -> Tuple.of(tuple.first(), tuple.first().get(id);
    }
    
    public static <D extends DB.GetSingleBy1<ID, ID, User>> Function<Tuple2<D, User>, User> getUserFinish(ID id) {
        return tuple -> tuple.first().get(id);
    }
}
```

We would be writing our logic in triplicate. So, it's very helpful to have `bind` available.

##### But wait, that looks too simple...

Of course, Java interfaces don't compose particularly well, and there is no good way to say "mapping a `Function<T, R>` f across a thing `F<T>` that implements `Functor<T>` or `Monad<T>` will always return an `F<R>` and not a `Functor<R>` or a `Monad<R>`". In java, that information is lost as soon as the interface is invoked.

Additionally, there's now way we can just *add* the functionality we want to an existing class, so we would have to either write our own or find somebody else's implementation. Luckily, there are a couple. In general, a Function used for this purpose (as a closure that reads some piece of contextual functionality or data before acting), is known as a `Reader`, and there are at least two implementations of `Reader` in existing libraries:

- [functionaljava](http://www.functionaljava.org/javadoc/4.8.1/functionaljava/fj/data/Reader.html)
- [cyclops](https://github.com/aol/cyclops/blob/master/cyclops-pure/src/main/java/cyclops/control/Reader.java), in which `bind` is known as `flatMap`

Using this knowledge, we can change our implementation of the `Service` class:

```java
import cyclops.control.Reader;

public class Service {
    public static Reader<? extends DB.Put<ID, User>, User> addUser(User user) {
        return db -> db.put(user);
    }
        
    public static Reader<? extends DB.GetSingleBy1<ID, ID, User>, User> getUser(ID id) {
        return db -> db.get(id);
    }
}
```

And the `main` method:

```java
public static void main() {
    ...
    
    User result = Service.addUser(user)
        .map(User::getId)
        .flatMap(Service::getUser)
        .apply(dbContext)
        ...
}
```

Notice how `Reader` can just be dropped in to replace `Function` in our Service class? Remember that, because it's foundational to how this whole thing hangs together. `Reader` is just `Function` with some extra methods tacked on!

Next:

#### Eager Typing, Lazily Implemented

Instead of looking at the entire composed function to work out that its type must, by definition, be `Function<? extends DB.Put<ID, User> & DB.GetSingleBy1<ID, ID, User>>`, i.e. a function that must be able to do all of the things we have just asked it to do, the compiler just infers the result of `Service.addUser(user)` to be `Function<DB.Put<ID, User>, User>` and complains when we then ask it to do something else. Thus, unless we explicitly tell it what type to expect, it will not let us compose that function with `Service.getUser`, because `DB.Put<ID, User>` and `DB.GetSingleBy1<ID, ID, User>` don't match. The first step to fixing this is that we have to replace the types `? extends DB.Put<ID, User>` and `? extends GetSingleBy1<ID, ID, User>` with explicit type parameters:

```java
public static class Service {
    public static <C extends DB.Put<ID, User>> Reader<C, User> addUser(User user) {
        return db -> db.put(user);
    }
    
    public static <C extends DB.GetSingleBy1<ID, ID, User>> Reader<C, User> getUser(ID id) {
        return db -> db.get(id);
    }
}
```

We do this so that the compiler knows we actually care what `?` is and we want to keep track of it for later use. The second step is that we have to explicitly tell the compiler that we will be using a `DBContext` and not just a `DB.Put<ID, User>` or a `DB.GetSingleBy1<ID, ID, User>`. We can do that in two ways. One way is to cast the result of `Service.addUser(user)` to the type we know it to be:

```java
addUser = (Reader<DBContext, User>) Service.addUser(user)
User res = addUser.mapResult
    etc...
```

That's ugly. Plus, it's brittle because if you break the name or signature of `DBContext`, you might not find out about it here until runtime. The other way is to write an `ask` function:

```java
class DBContext [...] {
    ...
    public static Reader<DBContext, DBContext> ask = Reader.of(Function.identity())
}
```

And then use it to start the process:

```
static void main() {
    ...
    User res = DBContext.ask
        .flatMap(__ -> Service.addUser(user))
        .map(User::getId)
        .flatMap(Service::getUser)
        .apply(dbContext);
    ...
}
```

This isn't pretty either, but it does work, and it has the advantage of being completely checked at compile-time to make sure the types all work out correctly. If you change the signature of `DBContext` to remove one of the two `DB` interfaces we need, you should get a nice new red line in your IDE letting you know that something is wrong.

Our final result will look something like [this](src/test/java/examples/SimpleExample.java)

### Let me get my head around this...

That's a fair amount of weirdness I'm asking you to ingest. The conceptual scaffolding surrounding this comes from category theory by way of [Haskell](https://hackage.haskell.org/package/mtl-1.1.0.2/docs/Control-Monad-Reader.html), but I don't expect you to read all about the [monad laws](https://wiki.haskell.org/Monad_laws) or what an Applicative is, or how to derive `map` from `bind in order to use the concepts or tools presented here. I want to make life easier for you, so you can go back to getting stuff done, so while there's a bunch of [stuff](https://github.com/louthy/language-ext/issues/515#issuecomment-433991418) I will suggest you read, I'd like to make this as easy to consume on its own as possible, so let me take a minute to address the elephant in the room.
    
### Why can't I just do `User savedUser = Service.addUser(user);`?

You might not have been thinking that yet, but you'll get there. Everybody gets there. So here's a little more of what's going on under the hood when we do this:

```java
User res = DBContext.ask
    .flatMap(__ -> Service.addUser(user))
    .map(User::getId)
    .flatMap(Service::getUser)
    .apply(dbContext);
```

#### What is this, communism?

What exactly are we asking for, here, and why are we asking? `ask` is another piece of terminology that comes from Haskell's Reader monad, and we're using it in this case in a slightly less-than-ideally-generic way, because cyclops' Reader doesn't have an ask method. If it did, it would look like:

```java
class Reader<C, V> {
    public static <T> Reader<T, T> ask() {
        return Reader.of(Function.identity());
    }
}
```

What we're doing there is just stating the beginning of what we expect to be a chain of monadic transformations. Using that method in our example above would look like:

```java
User res = Reader.<DBContext>ask()
    .flatMap(__ -> Service.addUser(user)) // Note that we're ignoring the argument
                                          // to this lambda. That's intentional!
    ...
```

Each transformation returns a reader:

```java
User res = Reader.<DBContext>ask()        // Reader<DBContext, DBContext>
    .flatMap(__ -> Service.addUser(user)) // Reader<DBContext, User>
    .map(User::getId)                     // Reader<DBContext, ID>
    .flatMap(Service::getUser)            // Reader<DBContext, User>
```

And as we noted above, a `Reader` is just a `Function` with some extra methods attached, so what we get at the end is equivalent to `Function<DBContext, User>`. If we could define it on the fly as a method, it would look something like this:
 
```java
public User doTheThing(DBContext db) {
    User addedUser = db.put(user);
    ID id = addedUser.getId();
    User gotUser = db.get(id);
    return gotUser;
}
```

It's still a function, though, which means we haven't actually *done* anything yet, except construct a slightly more complex function out of simpler functions. In order to get something out of it, we have to actually *call* the function on a `DBContext`, so we do:

```java
   .flatMap(Service::getUser)             // Reader<DBContext, User>
   .apply(dbContext)                      // User
```

And we get a user out, just as we expect.

#### Slow down, there pokey!

It took me a long time to get my head around all this, but the first epiphany came when I realized that what we are doing is building a function and *then* calling it. The next step was grokking how `bind` enables us to chain together what look like independent context readers. Why didn't we have to pass in two instances of the DB? Because we're not doing anything with the DB until the very end! All we're doing is building the function that will use the DB to do stuff. We use `bind` (or `flatMap`) in any instance where we're calling a new `Service` method because, instead of bringing in the requirement for a second database, what it brings in is a second *type requirement* from whatever we might pass in to satisfy our composed function.

Taking the annotated example above:

```java
User res = Reader.<DBContext>ask()        // Reader<DBContext, DBContext>
    .flatMap(__ -> Service.addUser(user)) // Reader<DBContext, User>
    .map(User::getId)                     // Reader<DBContext, ID>
    .flatMap(Service::getUser)            // Reader<DBContext, User>
    .apply(dbContext);                    // User
```

If we eliminate our foreknowledge of the `DBContext` type, here's what we're doing:

```java
User res = Reader.<? extends Object>ask() // Reader<? extends Object, ? extends Object>
    .flatMap(__ -> Service.addUser(user)) // Reader<? extends Object & DB.Put<ID, User>, User>
    .map(User::getId)                     // Reader<? extends Object & DB.Put<ID, User>, ID>
    .flatMap(Service::getUser)            // Reader<? extends Object & DB.Put<ID, User> & DB.GetSingleBy1<ID, ID, User>, User>
    .apply(dbContext);                    // User
```

Without knowing what `?` must be in advance, we compose our requirements from it over successive calls to `flatMap(Service::someMethod)`. Each new `Service` method we add, instead of requiring a new database instance, just adds a type requirement to the input of the function we are composing, until we get:

```java
? extends Object
        & DB.Put<ID, User>
        & DB.GetSingleBy1<ID, ID, User
```

which `dbContext` must then satisfy, or the code won't compile. Even though we must actually know what `DBContext` is in advance, we still have this exhaustive compile-time validation. If we remove the `get` method, for example, from the `DBContext` class, compile fails because `DBContext` doesn't satisfy its declared `DB.GetSingleBy1<ID, ID, User>` interface. If we remove the interface, compile *still* fails, because now we're passing something to our function chain that *doesn't* satisfy `DB.Put<ID, User> & DB.GetSingleBy1<ID, ID, User>`. Likewise, we can add a new static method to our `Service` class that wants new functionality from the database, but we can't pass `DBContext` into any `Reader` chain that uses that new method until `DBContext` satisfies whatever functionality is required.

This means that your `Service` class knows nothing about the database! It doesn't even know what *class* the database is! And there's no separate interface that we have to maintain in order to link the `Service` and the database together. All the `Service` knows or cares about is what functionality it wants. This also means that you can write a `Service` class *and* you can write exhaustive tests of the `Service` class with exactly zero knowledge of what the database backend is going to look like.

#### Something's fishy here. I can smell it.

This doesn't come entirely without a cost. I fact, it comes at two costs. The first is that we're constantly creating new function objects, which isn't free from the JVM's perspective. The second is that we can't just look at or modify the `User` using normal java language constructs and patterns any time we want. We have to wrap our interactions with the user in functional semantics like `map` and `flatMap`/`bind`, which, for people who aren't already accustomed to it, can be both hard to read and hard to reason about. As with any linguistic decision, this one should be taken with care. And we haven't even plumbed the full depth of its complexities yet.

#### What about failure?

For example, what happens if `Service.addUser(user)` should throw an exception? Remember, we can't wrap that individual call in a `try/catch` block, because *all we're doing is composing a function*. We haven't actually *done* anything until we call `apply`. We could wrap the whole thing in a `try/catch` block, but that's messy and doesn't give us great targeting. The FP world has an answer for this problem too, and it brings its own complications.

### Monads all the way down

The FP solution is to return a monadic `Try`. If we adjust our abstract definition of `DB`, it now looks like this:

```java
public class DB {
    public interface Record<ID> {
        ID getId();
    }

    @FunctionalInterface
    public static interface Put<ID, T extends Record<ID>> {
        Try<T, Throwable> put(T record);
    }

    @FunctionalInterface
    public static interface GetSingleBy1<ID, TFirstSelect, T extends Record<ID>> {
        Try<T, Throwable> get(TFirstSelect firstSelect);
    }
}
```

Try encapsulates the nondeterminism inherent in the possibility of failure, and at any given time a `Try` might contain **either** a value or an exception (`Throwable`, in this case). `Try` is also a functor and a monad, which means it has the same methods we used above, `map` and `flatMap`. 

But this breaks our `Service` class, it's expecting to get `User`s and it's getting `Try`s instead, so let's update it:

```java
public class Service {
    public static <C extends DB.Put<ID, User>> Reader<C, Try<User, Throwable>> addUser(User user) {
        return db -> db.put(user);
    }

    public static <C extends DB.GetSingleBy1<ID, ID, User>> Reader<C, Try<User, Throwable>> getUser(ID id) {
        return db -> db.get(id);
    }
}
```

That's fine and all, and it wasn't that hard to do, but *now* our main method is broken, because `Try` doesn't have a `getId` method:

```java
User res = DBContext.ask
    .flatMap(__ -> Service.addUser(user))
    .map(User::getId)           // <- Not a User anymore. What do we do here?
    .flatMap(Service::getUser)  // <- Or here, for that matter?
    .apply(dbContext);
```

There are two ways through this problem, the obvious but cumbersome one, and the less obvious, slightly more ugly, more abstract one. First, we can just *deal* with having a `Try` nested inside our `Reader`:

```java
Try<Model.User, Throwable> result = DBContext.ask
    .flatMap(__ -> Service.addUser(user))
    .map(tr -> tr.map(Model.User::getId))
    .map(tr -> tr.map(Service::getUser))  // We can't flatMap here anymore, because the types don't match up.
    .apply(dbContext)                     // Now we're down from Reader<DBContext, Try<Reader<DBContext, Try<User, Throwable>>, Throwable>>
                                          // To just Try<Reader<DBContext, Try<User, Throwable>>, Throwable>
                                          // So we can use flatMap in order to condense things further.
    .flatMap(reader -> reader.apply(dbContext));
// but -now- what do we do?
```

We have to handle both possible cases (success or failure) in order to close it out. In order to do that, we have a couple of options. We could use `bipeek(successFn, failureFn)`, or `orElse` with a default User. Either one will work. But there are more important problems. Notice how we had to call `apply(dbContext)` twice? That's unacceptable. It's simple enough in this (very) small problem scope, but it's a recipe for boilerplate and nonsense if we are forced to do that in order to get proper error handling in a real-world application.

There *is*, however, a solution to this problem, or at least, an answer.

### THE KLEISLI MONAD

Because, for real guys, names are a thing. `Kleisli` is a generic monad transformer. There are many monad transformers, but specifically this one lets us treat a `Reader<T, Try<R, X>>` as if it behaves like a `ReaderTry<T, R, X>`. Sort of; stay with me here. Remember that `Reader<T, R>` is synonymous with `Function<T, R>`? We're actually going to stop using `Reader` entirely, but we are going to use something else that is synonymous with `Function<T, R>`, and that something is `Kleisli`. We leave our abstract `DB` interface alone, but we *do* change our `Service` class (which is the first place we started using `Reader`):

```java
import com.oath.cyclops.hkt.Higher;
import static com.oath.cyclops.hkt.DataWitness.tryType;

public class Service {
    public static <C extends DB.Put<ID, User>> Kleisli<Higher<tryType, Throwable>, C, User> addUser(User user) {
        return Kleisli.of(TryInstances.monad(), db -> db.put(user));
    }

    public static <C extends DB.GetSingleBy1<ID, ID, User>> Kleisli<Higher<tryType, Throwable>, C, User> getUser(ID id) {
        return Kleisli.of(TryInstances.monad(), db -> db.get(id));
    }
}
```

Them's ugly. I know. But there's a method to the madness. `Kleisli` wants to use higher-kinded types in order to work (that is, types of types). In this case, we're using higher-kinded types to express a partial application of the `Try` type. We can't write `Try<Throwable>`, but that's actually what we want: this allows us to treat `Try<Throwable>` and `Reader<C>` as composable monads around (in this case) `User`. *Remember that `Reader` is a synonym for `Function`.* We don't actually *see* `Function` or `Reader` anywhere around here, but `Kleisli<Higher<tryType, Throwable>>` is in this case *also* a synonym for `Function`. Note the rest of the type expression; if we replace `Kleisli<Higher<tryType, Throwable>>` with `Function` we get `Function<C, User>`. For the skinny on `Higher` and what's going on with `tryType`, you can check out [this post](https://medium.com/@johnmcclean/simulating-higher-kinded-types-in-java-b52a18b72c74) by the author of cyclops.

There's more to do here. Note that our `Service` methods, which used to return a `Reader<C, Try>`, could pass the `try` through uninterrupted:

```java
public static <C extends DB.GetSingleBy1<ID, ID, User>> Reader<C, Try<User, Throwable>> getUser(ID id) {
    return db -> db.get(id);
}
```

Not true anymore. Now we have to tell the `Kleisli` what we're composing it with (in this case a `Try`):

```java
public static <C extends DB.GetSingleBy1<ID, ID, User>> Kleisli<Higher<tryType, Throwable>, C, User> getUser(ID id) {
    return Kleisli.of(TryInstances.monad(), db -> db.get(id));
}
```

#### More of that Haskell goodness

WTF is `TryInstance.monad()`, anyway? Well, it turns out that in addition to faking higher-kinded types, cyclops is *also* using faked HKTs to fake haskell-style typeclasses. You can think of a haskell typeclass as roughly analogous to an interface, but because of how haskell was designed, they're used in a different way. From an example in [Learn You A Haskell](http://learnyouahaskell.com/making-our-own-types-and-typeclasses#typeclasses-102)

```haskell
-- Here's our interface
class Eq a where
    (==) :: a -> a -> Bool
    (/=) :: a -> a -> Bool
    
-- And here's what in java would be referred to as a class, that is, a structure for some data
data TrafficLight = Red | Yellow | Green

-- And here's what ties the whole room together:
instance Eq TrafficLight where
    Red == Red = True
    Green == Green = True
    Yellow == Yellow = True
    _ == _ = False
```

Now, the power of this approach (and its downfall in our case, since we can't use it easily) is that all three of those declarations can be in separate files, or even separate libraries. That means I can have an interface I don't control, and a dataclass I don't control, and I can write my own implementation of that interface and attach it to that dataclass. I can also derive instances of a given typeclass for a dataclass with a given known property *without having to write any code at all*. We see this in haskell all the time. Here's a definition of `Maybe`, which is haskell's equivalent to Java's `Optional` type:

```
 data Maybe a = Just a | Nothing
     deriving (Eq, Ord)
```

You see that? That's the [whole thing](https://wiki.haskell.org/Maybe). I just got a consistent, standards-compliant `equals()` method without having to work for it! Not to mention `compare()`. In Java, I would have to use Lombok for that. In haskell, it's part of the type system. Cyclops is trying to bring some of that power down to our level, and it seems to at least partially work. In this case, `Kleisli` wants a `Monad` instance (in the Haskell sense, that is, a description of how some type `T` behaves like a monad) and a function that returns the monad in question. From `Kleisli`'s source code:

```java
public static <W,T,R> Kleisli<W,T,R> of(Monad<W> monad, Function<? super T, ? extends Higher<W,? extends R>> fn){
    return new Kleisli<W,T,R>(monad,fn);
}
```

In our case, `W` is `Higher<tryType, Throwable>`, and we get a `Monad<W>` by calling `TryInstances.monad()`. As with many of our inroads from imperative java-land to functional abstraction, this one isn't as pretty as we would like it to be. The good news is, we don't have to call `TryInstances.monad()` anywhere *except* the `Service` methods.

#### That's a lotta nuts

What does all that mean? It means we can start updating our `main` method. Well, first, we have to update our `ask`, and I think it now makes more sense to call it `askK`, since we're asking for a `Kleisli` and not just a `Reader`:

```java
    public static Kleisli<Higher<tryType, Throwable>, DBContext, DBContext> askK =
        Kleisli.of(TryInstances.monad(), Try::success);
```

*Now* we can start updating our `main` method. `Kleisli` gives us access to some methods we haven't seen before, but the ones we're familiar with still behave in ways we recognize. This won't work, for example:

```java
DBContext.askK
    .flatMap(__ -> Service.addUser(user))
    
// Incompatible equality constraint: tryType and Higher<kleisli, Higher<tryType, Throwable>>
```

That's because we're still treating it like a `Reader` or a `Function`, but we're using it because we want to treat it like a `Kleisli`. It gives us methods for that; in this case, we want to trade `flatMap` for `flatMapK` (because we're `flatMap`ing a `Kleisli` and not just any old `Function`):

```java
DBContext.askK
    .flatMapK(__ -> Service.addUser(user))
    
// Works!
```

`map` still works on the inner `User` instead of the `Try<User, Throwable>` (and we don't have a `mapK` function), which seems inconsistent to me, but it's what we've got, so I'll roll with it:

```java
User res = DBContext.askK
    .flatMapK(__ -> Service.addUser(user))
    .map(User::getId)
    .flatMapK(Service::getUser)
    .apply(dbContext);
```

This still doesn't work though, because when we `apply` our `Kleisli`, we get back `Higher<Higher<tryType, Throwable>, User>`. Well, *that's* not what we want. `Higher<Higher<tryType, Throwable>, User>` is the higher-kinded encoding of `Try<User, Throwable>`.

#### Narrowing our expectations

The good news is that because cyclops knows we will need to convert back and forth between types and their HKT encodings, we have access to the static method `Try.narrowK`, which we can use in one of two ways. The obvious way:

```java
Try<User, Throwable> res = Try.narrowK(
    DBContext.askK
        .flatMapK(__ -> Service.addUser(user))
        .map(User::getId)
        .flatMapK(Service::getUser)
        .apply(dbContext));
```

We can wrap the entire chain (or more accurately, its result) in `Try.narrowk`. But there's another way. We can make `Try.narrowK` part of the chain:

```java
Try<User, Throwable> res = DBContext.askK
    .flatMapK(__ -> Service.addUser(user))
    .map(User::getId)
    .flatMapK(Service::getUser)
    .andThen(Try::narrowK)
    .apply(dbContext);
```

And *now* all we have to worry about is simply handling our `Try`:

```java
res.biPeek(
    System.out::println,
    System.out::println
);
```

In this toy example, we can just print the result or print the failure. IRL, you can use `orElse` or any number of other methods on `Try` to account in different ways for the possibility of failure. I encourage you to experiment with them. You can see the full final code for all this [here](src/test/java/examples/Example.java). It's what I used to develop and refine my understanding of this approach, which yet remains a work in progress.

### Wrapping up.

So, this technique is not pretty. I'm aiming to mitigate that a little with this library (which as you can see is *currently* just a couple of examples, since I'm letting cyclops do all the heavy lifting and still working out how I want to approach this), because I believe that even though it's not pretty, the approach has merit. Testable static service classes! Separation of concerns at the method level! Compile-time checks on your service dependency graph! But without alterations to the underlying language, it's -never- going to be pretty. 

#### Post-script

Haskell provides two powerful tools to get around the otherwise somewhat ugly syntax of monad chaining. The first is that you can write your own infix operators, which means `Kleisli<Higher<tryType, Throwable>>` just looks like `>=>`, and `.flatMap(Service::getUser)` looks like `>>= Service.getUser`. The second is `do` notation:

```haskell
let process = do
    addedUser <- Service.addUser user
    gotUser <- Service.getUser $ id addedUser
    return gotUser
in
    runReader process dbContext
```

Or something like that, anyway. The point is, `do` unwraps monads as you go, which makes it much cleaner to look at.

##### Optional side trip!

I don't generally advocate for bits of syntax being built into the language in the way of `do` notation in haskell or `for` comprehensions in scala (both aimed at making monads easier to deal with); my belief is that as much as possible should arise naturally from the underlying structure of the language. For example, I'm a huge fan of F#'s forward pipe operator (`|>`; `foo(bar(baz))` is equivalent to `baz |> bar |> foo`) because I think it enables me to write code that's easy to read and reflects the way that we think. I can trivially define `|>` in scala or haskell (or F#, if it wasn't already there), and indeed all of the infix operators available in those languages are *just* ordinary functions, and anybody can arbitrarily define their own. But I can't define it in python, because python's infix operators (and Java's) are hard-coded into the language. So too with `try/catch` blocks, and especially checked exceptions; my view is that exception handling should arise from existing properties of the language (like the type system, in this case) instead of requiring special syntax to be hardcoded in.

This is of course not always possible, or at least, not always knowably possible (in the same way that type-level exception handling wasn't known to be possible when Java's checked exceptions were developed), and it seems pretty clear to me that while there will at some point be a way for pretty monad handling to be an emergent property of some language `foo`, nobody knows how to make that property happen right now, and so, like with `try` statements in earlier eras of programming, we are forced to fall back on hard-coded syntax in the form of `do` notation and `for` comprehensions.

To that extent, I am okay with it. Moreso because monads appear to be an extremely broad abstraction, covering a huge variety of different use cases; literally anything you can express in terms of `thing a` being 'contained' or 'wrapped' by `thing b`, from `List`s all the way up to Free Monads and grammar parsers, including dependency injection and a host of other abstractions besides. So, if I have to compromise my dogmatic sense of language purity, at least I'll get a lot of mileage out of it.
