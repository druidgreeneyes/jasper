package examples;

import java.util.ArrayList;
import java.util.function.Function;
import java.util.HashMap;
import java.util.HashSet;

public class Misc {
    public static class Mapping {
        public static <T, R> ArrayList<R> map(ArrayList<T> list, Function<? super T, ? extends R> fn) {
            ArrayList<R> res = new ArrayList<>();
            list.forEach(t -> res.add(fn.apply(t)));
            return res;
        }

        public static <K, T, R> HashMap<K, R> map(HashMap<K, T> map, Function<? super T, ? extends R> fn) {
            HashMap<K, R> res = new HashMap<>();
            map.forEach((k, t) -> res.put(k, fn.apply(t)));
            return res;
        }

        public static <T, R> HashSet<R> map(HashSet<T> set, Function<? super T, ? extends R> fn) {
            HashSet<R> res = new HashSet<>();
            set.forEach(t -> res.add(fn.apply(t)));
            return res;
        }

        public static <I, T, R> Function<? super I, ? extends R> map(Function<? super I, ? extends T> fn, Function<? super T, ? extends R> mapper) {
            return fn.andThen(mapper);
        }
    }

    public static class Binding {
        public static <T, R> ArrayList<R> bind(ArrayList<T> list, Function<? super T, ? extends ArrayList<R>> fn) {
            ArrayList<R> res = new ArrayList<>();
            list.forEach(t -> res.addAll(fn.apply(t)));
            return res;
        }

        public static <K, T, R> HashMap<K, R> bind(HashMap<K, T> map, Function<? super T, ? extends HashMap<K, R>> fn) {
            HashMap<K, R> res = new HashMap<>();
            map.forEach((k, t) -> res.putAll(fn.apply(t)));
            return res;
        }

        public static <T, R> HashSet<R> bind(HashSet<T> set, Function<? super T, ? extends HashSet<R>> fn) {
            HashSet<R> res = new HashSet<>();
            set.forEach(t -> res.addAll(fn.apply(t)));
            return res;
        }

        public static <I, T, R> Function<? super I, ? extends R> map(Function<? super I, ? extends T> fn, Function<? super T, ? extends Function<? super I, ? extends R>> binder) {
            return i -> binder.apply(fn.apply(i)).apply(i);
        }
    }

    public static interface Functor<T> {
        public <R> Functor<R>  map(Function<? super T, ? extends R> fn);
    }

    public static interface Monad<T> extends Functor<T> {
        public <R> Monad<R> bind(Function<? super T, ? extends Monad<? extends R>> fn);
    }
}
