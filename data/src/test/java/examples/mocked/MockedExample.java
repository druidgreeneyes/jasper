package examples.mocked;

import java.util.function.Consumer;

import com.oath.cyclops.hkt.DataWitness.tryType;
import com.oath.cyclops.hkt.Higher;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import cyclops.arrow.Kleisli;
import cyclops.control.Option;
import cyclops.control.Try;
import cyclops.data.HashMap;
import cyclops.instances.control.TryInstances;
import jasper.core.annotations.Record;
import jasper.core.annotations.wrapper.Wrapped;
import jasper.core.annotations.wrapper.Wrapper;
import jasper.data.DB;
import jasper.data.DBRecord;

public class MockedExample {
    /*
    This is our model, with a user and an ID class.
     */
    @Wrapped
    public static abstract class _UserID extends Wrapper<Long>{}

    @Record
    public static interface _User extends DBRecord<UserID> {
        String name();
    }

    // Service Methods
    public static class Service {
        public static <C extends DB.Put<User>> Kleisli<Higher<tryType, Throwable>, C, User> addUser(User user) {
            return Kleisli.of(TryInstances.monad(), db -> db.put(user));
        }

        public static <C extends DB.Get<User>> Kleisli<Higher<tryType, Throwable>, C, User> getUser(User match) {
            return Kleisli.of(TryInstances.monad(), db -> db.get(match));
        }
    }

    // Main
    public static class Main {
        private static class UserAlreadyExistsException extends Exception {}
        private static class UserDoesNotExistException extends Exception {}

        private static class Context implements DB.Put<User>, DB.Get<User> {
            public Context() {}

            private final java.util.HashMap<UserID, User> db = new java.util.HashMap<>();

            public Try<User, Throwable> put(User user) {
                if (db.containsKey(user.id())) {
                    return Try.failure(new UserAlreadyExistsException());
                } else {
                    db.put(user.id(), user);
                    return Try.success(user);
                }
            }

            public Try<User, Throwable> get(User match) {
                return Option.ofNullable(db.get(match.id()))
                    .toTry(new UserDoesNotExistException());
            }

            public static Kleisli<Higher<tryType, Throwable>, Context, Context> askK =
                    Kleisli.of(TryInstances.monad(), Try::success);
        }


        public void main() {
            Context context = new Context();

            User user = User.of(
                    UserID.of(1L),
                    "John Woo"
            );

            Context.askK
                    .flatMapK(__ -> Service.addUser(user))
                    .flatMapK(Service::getUser)
                    .andThen(Try::narrowK)
                    .apply(context)
                    .bipeek(System.out::println, System.out::println);
        }
    }

    // Tests
    public static class UserServiceTests {
        private final HashMap<UserID, User> db = HashMap.empty();

        private final User testUser = User.of(UserID.of(1L), "test");
        private final Consumer<User> assertSuccessfulUser = u -> Assertions.assertEquals(u, testUser);
        private final Consumer<Throwable> fail = th -> Assertions.fail();

        @Test
        public void putUserTest() {
            final DB.Put<User> putter = user -> db.put(user.id(), user).get(user.id()).toTry(new Exception());

            // Validate that our putter works
            putter.put(testUser).bipeek(
                    // Success
                    assertSuccessfulUser,
                    // Failure
                    fail
            );

            // Test addUser
            Service.addUser(testUser)
                    .andThen(Try::narrowK)
                    .apply(putter)
                    .bipeek(
                            // Success
                            assertSuccessfulUser,
                            // Failure
                            fail
                    );
        }

        @Test
        public void getUserTest() {
            final DB.Get<User> getter = user -> db.put(testUser.id(), testUser).get(user.id()).toTry(new Exception());

            // Validate that our getter works
            getter.get(testUser).bipeek(
                    // Success
                    assertSuccessfulUser,
                    // Failure
                    fail
            );

            // Test getUser()
            Service.getUser(testUser)
                    .andThen(Try::narrowK)
                    .apply(getter)
                    .bipeek(
                            // Success
                            assertSuccessfulUser,
                            // Failure
                            fail
                    );
        }
    }
}
