package examples.h2;

import cyclops.control.Reader;
import jasper.core.annotations.Record;
import jasper.core.annotations.wrapper.Wrapped;
import jasper.core.annotations.wrapper.Wrapper;
import jasper.data.DB;
import jasper.data.DBRecord;

import java.awt.image.DataBufferUShort;
import java.util.function.Function;

public class H2Example {

    public static class Model {

        @Wrapped
        public static abstract class _UserId extends Wrapper<Integer> {}

        @Wrapped
        public static abstract class _AccountId extends Wrapper<Integer> {}

        @Record
        public interface _User extends DBRecord<UserId> {
            String name();
            AccountId accountId();
        }

        @Record
        public interface _Account extends DBRecord<AccountId> {
            int commentCount();
        }
    }

    public static class Service {
        @FunctionalInterface
        public static interface DBGetUserById extends DB.Get<Model.User> {
            default User getUser(User match) {
                return get(match);
            }
        }

        @FunctionalInterface
        public static interface DBGetAccountById extends DB.Get<Account> {
            default Account getAccount(AccountId id) {
                return get(id);
            }
        }

        public static <C extends DB.Put<Model.UserId, Model.User>> Reader<C, Model.User> addUser(Model.User user) {
            return db -> db.put(user);
        }

        public static <C extends DB.GetSingleBy1<Model.UserId, Model.UserId, Model.User>> Reader<C, Model.User> getUser(Model.UserId id) {
            return db -> db.get(id);
        }

        public static <C extends DBGetUserById & DBGetAccountById> Function<C, Model.Account> getUserAccount(Model.UserId userId) {
            return (C db) -> {
                Model.User user = db.getUser(userId);
                Model.Account account = db.getAccount(accountId);
                return account;
            };
        }
    }

    

    public static class DBContext
            implements
            DB.Put<Model.UserId, Model.User>,
            DB.GetSingleBy1<Model.UserId, Model.UserId, Model.User>{
                
        private final DBConnection conn = Database.connect(connectionString);

        public Model.User get(Model.UserId id) {
            return conn.getByUserId(id);
        }

        public Model.User put(Model.User user) {
            conn.put(user);
            return user;
        }

        public static final Reader<DBContext, DBContext> ask = Reader.of(Function.identity());
    }

    public static class Main {
        public static void main() {
            DBContext dbContext = DBContext.connect("sqlite:///local.db");

            Model.User user = new Model.User(
                    "John Woo",
                    Model.UserId.of(1)
            );

            Model.User result = DBContext.ask
                    .flatMap(__ -> Service.addUser(user))
                    .map(Model.User::getId)
                    .flatMap(Service::getUser)
                    .apply(dbContext);

            System.out.println(result.toString());

        /*
        Should print out:
        {"name": "John Woo", "id": {"id": 1}}
        */
        }
    }
}
