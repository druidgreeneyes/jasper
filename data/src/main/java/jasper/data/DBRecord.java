package jasper.data;


public interface DBRecord<T> {
    T id();
}
