package jasper.data;

import cyclops.control.Try;
import cyclops.data.Seq;

public final class DB {
    private DB() {}

    @FunctionalInterface
    public static interface Put<T extends Record<?>> {
        Try<T, Throwable> put(T t);
    }

    @FunctionalInterface
    public static interface PutMany<T extends Record<?>> {
        Try<Seq<T>, Throwable> put(Seq<T> ts);
    }

    @FunctionalInterface
    public static interface Get<T extends Record<?>> {
        Try<T, Throwable> get(T match);
    }

    public static interface GetMany<T extends Record<?>> {
        Try<Seq<T>, Throwable> get(Seq<T> match);
    }

    @FunctionalInterface
    public static interface Delete<T extends Record<?>> {
        Try<T, Throwable> delete(T match);
    }

    @FunctionalInterface
    public static interface DeleteMany<T extends Record<?>> {
        Try<Seq<T>, Throwable> delete(Seq<T> match);
    }
}
