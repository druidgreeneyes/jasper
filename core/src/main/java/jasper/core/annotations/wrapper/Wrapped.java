package jasper.core.annotations.wrapper;

import java.lang.annotation.Target;
import java.lang.annotation.ElementType;

import org.immutables.value.Value;

@Target(ElementType.TYPE)
@Value.Style(
    // Detect names starting with underscore
        typeAbstract = "_*",
    // Generate without any suffix, just raw detected name
        typeImmutable = "*",
        allParameters = true,
    // Make generated public, leave underscored as package private
        visibility = Value.Style.ImplementationVisibility.PUBLIC,
    // Seems unnecessary to have builder or superfluous copy method
        defaults = @Value.Immutable(builder = false, copy = false))
public @interface Wrapped {}
