package jasper.core.annotations.wrapper;

public abstract class Wrapper<T> {
    public abstract T value();

    @Override
    public String toString() {
        return String.format("%s(%s)", this.getClass().getSimpleName(), value());
    }
}
