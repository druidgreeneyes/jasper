package jasper.core.annotations;

import java.lang.annotation.Target;
import java.lang.annotation.ElementType;

import org.immutables.value.Value;

@Target(ElementType.TYPE)
@Value.Style(
    // Detect names starting with underscore
    typeAbstract = "_*",
    // Generate without any suffix, just raw detected name
    typeImmutable = "*",
    allParameters = true, // all attributes will become constructor parameters
                          // as if they are annotated with @Value.Parameter
    visibility = Value.Style.ImplementationVisibility.PRIVATE, // Generated class will be always public
    defaults = @Value.Immutable(builder = false)
) // Disable copy methods and builder
public @interface Record {}
